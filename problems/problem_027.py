# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(*values):
    if len(values) == 0:
        return None

    return max(values)

def max_in_list3(*values):
    if len(values) == 0:
        return None
    maximum = values[0]
    for i in values:
        if i > maximum:
            maximum = i
    return maximum

print(max_in_list(10,9,8,7,6,5))

print(max_in_list3(10,9,8,7,6,5))
