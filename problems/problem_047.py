# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    contains_lower = False
    contains_upper = False
    contains_special = False
    contains_digit = False
    correct_length = False
    length = len(password)
    special_characters = ["$", "!", "@"]
    if (length >= 6 and length <= 12):
        correct_length = True
    for character in password:
        if character in special_characters:
            contains_special = True
        elif character.isdigit():
            contains_digit = True
        elif character.isalpha():
            if character.isupper():
                contains_upper = True
            elif character.islower(): 
                contains_lower = True
    return (
        contains_lower and contains_digit and contains_special and contains_upper and correct_length)

print(check_password("password"))
print(check_password("P@ssw0rd"))
