# Complete the is_inside_bounds function which takes an x
# coordinate and a y coordinate, and then tests each to
# make sure they're between 0 and 10, inclusive.

def is_inside_bounds(x, y):
    x_coordinate = x
    y_coordinate = y

    if x_coordinate in range(0,11) and y_coordinate in range(0,11):
        return True
    else: 
        return False
    


print(is_inside_bounds(2,2))
print(is_inside_bounds(11,2))
