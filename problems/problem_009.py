# Complete the is_palindrome function to check if the value in
# the word parameter is the same backward and forward.
#
# For example, the word "racecar" is a palindrome because, if
# you write it backwards, it's the same word.

# It uses the built-in function reversed and the join method
# for string objects.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

#     #split()reversed()join()
#     #if word == word
#         #return true; its a palidone
#     #else: return false

def is_palindrome(word):
    if word == word[::-1]:
        print ("Yes! It's a palidrome!")
        return True
        
    else:
        print ("Sorry, no. Try again.")
        return False


    
print(is_palindrome('racecar'))     #true
print(is_palindrome('baby'))        #false

#Use for in loop to iterate each letter in parameter using list()
#Use built-in reversed() on word
#Use join method for str obj
#if word is palindrome, set it equal to true
    #else: return false

