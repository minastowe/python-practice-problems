# Write a function that meets these requirements.
#
# Name:       sum_fraction_sequence
# Parameters: a number
# Returns:    the sum of the fractions of the
#             form 1/2+2/3+3/4+...+number/number+1
#
# Examples:
#     * input:   1
#       returns: 1/2
#     * input:   2
#       returns: 1/2 + 2/3
#     * input:   3
#       returns: 1/2 + 2/3 + 3/4

# def sum_fraction_sequence (number):
#     total = 0
#     for num in range(1, number + 1):
#         total += num / (num + 1)
#     return total

def sum_fraction_sequence(n):           # solution
    sum = 0                             # solution
    for i in range(1, n + 1):           # solution
        sum += i / (i + 1)              # solution
    return sum                          # solution


print(sum_fraction_sequence(3))
    
    
    
    
#for num in range(num)