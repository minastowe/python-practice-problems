# Complete the max_of_three function so that returns the
# maximum of three values.
#
# If two values are the same maximum value, return either of
# them.
# If the all of the values are the same, return any of them
#
# Use the >= operator for greater than or equal to

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def max_of_three(value1, value2, value3):
    #Use if statement to compare with value1 being greatest
    if value1 > value2 and value1 > value3:
        return value1
    #Use if statement to compare with value2 being greatest
    if value2 > value1 and value2 > value3:
        return value2
     #Use if statement to compare with value3 being greatest
    if value3 > value1 and value3 > value2:
        return value3
    elif value1 == value2 or value2 == value3 or value1 == value3:
        return value1 or value2 or value3
    elif value1 == value2 == value3:
        return value1 or value2 or value3
    else:
        return ("Nope didnt work. Try again!")

#Return the max out of all three parameters

#Print function using 3 arguments to see if we did it correctly!
print(max_of_three(1,2,3))
print(max_of_three(3,2,1))
print(max_of_three(1,3,2))
print(max_of_three(3,3,3))
print(max_of_three(3,2,3))


## Much easier and simplier version below from Solution 
## Also utilizes >= sign as well

# def max_of_three(value1, value2, value3):
#     if value1 >= value2 and value1 >= value3:                   # solution
#         return value1                                           # solution
#     elif value2 >= value1 and value2 >= value3:                 # solution
#         return value2                                           # solution
#     else:                                                       # solution
#         return value3                                           # solution
#     # pass                                                      # problem