# Write a function that meets these requirements.
#
# Name:       generate_lottery_numbers
# Parameters: none
# Returns:    a list of six random unique numbers
#             between 1 and 40, inclusive
#
# Example bad results:
#    [4, 2, 3, 3, 1, 5] duplicate numbers
#    [1, 2, 3, 4, 5] not six numbers
#
# You can use randint from random, here, or any of
# the other applicable functions from the random
# package.
#
# https://docs.python.org/3/library/random.html

import random

def generate_lottery_numbers():
    lottery_number = []
    
    while len(lottery_number) < 6: 
        random_number = random.randint(1, 40)
        if random_number not in lottery_number:
            lottery_number.append(random_number)
    return lottery_number

    # set lottery number to empty list
    # generate random number 6 times using loop
    #Make sure there are no repeats
    # append random number to empty lottery number list
    # return lottery number
    
print(generate_lottery_numbers())
print(generate_lottery_numbers())
print(generate_lottery_numbers())
print(generate_lottery_numbers())
print(generate_lottery_numbers())
print(generate_lottery_numbers())
print(generate_lottery_numbers())
print(generate_lottery_numbers())
print(generate_lottery_numbers())
print(generate_lottery_numbers())
