# Complete the minimum_value function so that returns the
# minimum of two values.
#
# If the values are the same, return either.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def minimum_value(value1, value2):
    #Use if statement to compare and return lesser value
    if value1 < value2:
        return value1
    #Use Or keyword if values are the same    
    elif value1 == value2:
        return value1 or value2
    #Use else statement for opposite
    else:
        return value2

#Return min of two values
# Print function using arguments to see if I did it right!   
print(minimum_value(5,10))
print(minimum_value(10,1))
print(minimum_value(6,6))
