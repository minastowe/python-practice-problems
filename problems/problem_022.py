# Complete the gear_for_day function which returns a list of
# gear needed for the day given certain criteria.
#   * If the day is not sunny and it is a workday, the list
#     needs to contain "umbrella"
#   * If it is a workday, the list needs to contain "laptop"
#   * If it is not a workday, the list needs to contain
#     "surfboard"

def gear_for_day(is_workday, is_sunny):
    lst_for_day = []
    if is_workday == True and is_sunny == False:
        lst_for_day.append("Umbrella")
    elif is_workday == True and is_sunny == True:
        lst_for_day.append("Laptop")
    else:
        lst_for_day.append("Surfboard")
    return "You need the following for today:" + str(lst_for_day)
    # if workday return laptop
    #if workday and not sunny return umbrella and laptop
    # if not work day, return surfboard

print(gear_for_day(True, False))
print(gear_for_day(True, True))
print(gear_for_day(False, False))
print(gear_for_day(False, True))