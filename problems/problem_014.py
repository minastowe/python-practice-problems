# Complete the can_make_pasta function to
# * Return true if the ingredients list contains
#   "flour", "eggs", and "oil"
# * Otherwise, return false
#
# The ingredients list will always contain three items.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def can_make_pasta(ingredients):
    result = 0
    for i in ingredients:
        if i == "flour" or i == "eggs" or i == "oil":
            result+=1
    return result == 3
   
    # create empty lst to push ingredients into
    #if can make pasta contains str of flour, eggs, and oil
        #return true;
    #else return false

print(can_make_pasta(["flour", "eggs", "oil"]))
print(can_make_pasta(["flour", "eggs", "water"]))
print(can_make_pasta(["water", "taco meat", "cheese"]))