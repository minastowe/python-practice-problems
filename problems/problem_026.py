# Complete the calculate_grade function which accepts
# a list of numerical scores each between 0 and 100.
#
# Based on the average of the scores, the function
# returns
#   * An "A" for an average greater than or equal to 90
#   * A "B" for an average greater than or equal to 80
#     and less than 90
#   * A "C" for an average greater than or equal to 70
#     and less than 80
#   * A "D" for an average greater than or equal to 60
#     and less than 70
#   * An "F" for any other average

def calculate_grade(*values):
    if len(values) == 0:
        return None
    grade = 0
    for x in values:
        grade += x
    average_grade = grade / len(values)
    if average_grade >= 90:
        return "A"
    elif average_grade >= 80 and average_grade < 90:
        return "B"
    elif average_grade >= 70 and average_grade < 80:
        return "C"
    elif average_grade >= 60 and average_grade < 70:
        return "D"
    else:
        return "F"

print(calculate_grade(100,100,100,100,100))
print(calculate_grade(90,90,90,90,90))
print(calculate_grade(80,80,80,80,80))
print(calculate_grade(70,70,70,70,70))
print(calculate_grade(60,60,60,60,60))
print(calculate_grade(50,50,50,50,50))
print(calculate_grade(1,2,3,4,5))
print(calculate_grade(0))