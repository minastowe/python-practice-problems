# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

def calculate_average(*values):
    if len(values) == 0:
        return None
    sum = 0
    for x in values:
        sum += x
    new_sum = sum / len(values)
    return new_sum
    

print(calculate_average(5,5,5,5,5))
print(calculate_average(15,25,2,3,5,7,9,10))
print(calculate_average(0))


#Calc sum of list first
#divide by len of list
#Sum equals/returns calculate average function

